from requests.exceptions import ConnectionError
from typing import List
from PIL import Image
import requests
import logging
import io
import os


def get_filename(url: str) -> str:
    filename: str = url.split("/")[-1]
    return filename if filename.lower().endswith("jpg") else filename.replace(filename.split(".")[-1], "jpg")


def process_image(content: bytes) -> Image:
    with Image.open(io.BytesIO(content)) as img:
        if img.mode in ("RGBA", "P"):
            img: Image = img.convert("RGB")
        width: int
        height: int
        width, height = img.size
        if width < 1000 and height < 1000:
            image_to_save: Image = img
        else:
            image_to_save: Image = img.resize(
                (width if width < 1000 else 1000, height if height < 1000 else 1000))
    return image_to_save


def save_images(urls: List[str], directory: str) -> List[str]:
    if not os.path.isdir(directory):
        raise NotADirectoryError("Provided directory is invalid")
    paths: List = []
    for url in urls:
        try:
            headers: dict = requests.head(url, allow_redirects=True).headers
            content_type: str = headers.get("content-type")
            if "image" in content_type:
                try:
                    content: bytes = requests.get(url, allow_redirects=True).content
                    image_to_save: Image = process_image(content)
                    filename: str = get_filename(url)
                    path: str = os.path.join(directory, filename)
                    with open(path, "wb") as file:
                        image_to_save.save(file)
                        paths.append(path)
                except Exception as e:
                    logging.warning(e)
        except ConnectionError:
            logging.warning('Provided URL is not valid')
    return paths
