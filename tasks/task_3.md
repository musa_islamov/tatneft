**Алгоритм**

Необходимо реализовать очередь с транзакциями, в которую Источник будет записывать необходимую информацию об изменениях
своего состояния. Клиенты подписываются на очередь и получают необходимые обновления. При этом, если сообщение с транзакцией по какой-то
причине не обработалось клиентом, необходимо отправить сообщение обратно в очередь (очередь отправки, или выделенную очередь для необработанных сообщений).
Для реализации данного подхода можно использовать брокер сообщений RabbitMQ.
Схема работы:
![схема работы](./схема_работы.png)

1) Источник отправляет сообщения с транзакциями на обменник
2) Обменник отправляет сообщения в очередь
3) Клиенты получают сообщения и обрабатывают их

При этом, обменник можно разместить на выделенном сервере, чтобы снизить нагрузку на источник

*Используемые инструменты:*
- Docker
- RabbitMQ
- Pika: клиентская библиотека для Python для работы с RabbitMQ